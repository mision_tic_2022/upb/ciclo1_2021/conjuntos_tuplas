#CONJUNTOS
set_placas_1: set = {"RJH-545", "ZMN-821", "MLK-981", "IKJ-987"}
set_placas_2: set = {"RJH-545", "MLK-981", "ASD-986", "IYS-981", "JHT-986"}

#Punto 1
#Obtener las placas que se encuentran en los dos conjuntos
resultado1 = set_placas_1.intersection(set_placas_2)
print (resultado1)

#Punto 2
#Obtener los elementos de 'set_placas_1' que no están en 'set_placas_2'
resultado2 = set_placas_1 - set_placas_2
print (resultado2)
#Punto 3
#Crear un conjunto con todos los elementos de los dos conjuntos
resultado3=set_placas_1 and set_placas_2
print(resultado3)

"""
Miguel
"""
#CONJUNTOS
set_placas_1: set = {"RJH-545", "ZMN-821", "MLK-981", "IKJ-987"}
set_placas_2: set = {"RJH-545", "MLK-981", "ASD-986", "IYS-981", "JHT-986"}

#Punto 1
#Obtener las placas que se encuentran en los dos conjuntos

set_placas=set_placas_1.intersection(set_placas_2)
print(set_placas)
#Punto 2
#Obtener los elementos de 'set_placas_1' que no están en 'set_placas_2'
resta_placas=set_placas_1-set_placas_2
print(resta_placas)

#Punto 3
#Crear un conjunto con todos los elementos de los dos conjuntos
suma_conjunto=set_placas_1.union(set_placas_2)
print(suma_conjunto)