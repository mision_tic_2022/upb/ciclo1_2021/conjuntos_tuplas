
tupla_numeros: tuple = (1,2,3,4,1)
print(tupla_numeros)

print(tupla_numeros[0])
#Las tuplas sin inmutables
#tupla_numeros[0] = 1 -> No hacer
#Contar cuantes veces está un elemento en la tupla
print(tupla_numeros.count(1))
#Obtener el indice de un elemento
print(tupla_numeros.index(3))