
"""
Ejercicio: 
"""

#CONJUNTOS
set_placas_1: set = {"RJH-545", "ZMN-821", "MLK-981", "IKJ-987"}
set_placas_2: set = {"RJH-545", "MLK-981", "ASD-986", "IYS-981", "JHT-986"}

#Punto 1
#Obtener las placas que se encuentran en los dos conjuntos

#Punto 2
#Obtener los elementos de 'set_placas_1' que no están en 'set_placas_2'

#Punto 3
#Crear un conjunto con todos los elementos de los dos conjuntos